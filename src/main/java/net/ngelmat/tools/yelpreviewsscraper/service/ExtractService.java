package net.ngelmat.tools.yelpreviewsscraper.service;

import java.io.IOException;
import java.util.List;

import net.ngelmat.tools.yelpreviewsscraper.domain.Review;

public interface ExtractService {

  List<Review> listReviews(String url) throws IOException;
}
