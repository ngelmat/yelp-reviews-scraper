package net.ngelmat.tools.yelpreviewsscraper.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import net.ngelmat.tools.yelpreviewsscraper.config.GoogleApiConfig;
import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request.VisionRequest;
import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request.VisionRequestBuilder;
import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response.GoogleApiResponse;
import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response.VisionResponse;
import net.ngelmat.tools.yelpreviewsscraper.service.GoogleVisionService;
import net.ngelmat.tools.yelpreviewsscraper.utils.JsonUtils;

@Service("googleVisionService")
public class GoogleVisionServiceImpl implements GoogleVisionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(GoogleVisionServiceImpl.class);

	@Autowired
	private GoogleApiConfig config;

	@Override
	public VisionResponse getEmotion(String imageUrl) {
		VisionRequest visionRequest = VisionRequestBuilder.createEmotionRequest(imageUrl);
		String jsonRequest = JsonUtils.toJson(visionRequest);
		LOGGER.info("Google vision api request sent: {}", jsonRequest);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<GoogleApiResponse> rawResult = restTemplate.postForEntity(config.getVisionEndpointUrl(), visionRequest, GoogleApiResponse.class);

		LOGGER.info("Google vision api response received: {}", rawResult);

		if (rawResult.hasBody()) {
			GoogleApiResponse response = rawResult.getBody();
			if (response != null && !response.getResponses().isEmpty()) {
				return response.getResponses().get(0);
			}
		}

		return null;
	}
}
