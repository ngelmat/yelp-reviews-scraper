package net.ngelmat.tools.yelpreviewsscraper.service;

import java.io.IOException;

import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response.VisionResponse;

public interface GoogleVisionService {

	VisionResponse getEmotion(String imageUrl) throws IOException;
}
