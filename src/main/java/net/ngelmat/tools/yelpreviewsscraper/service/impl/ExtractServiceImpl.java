package net.ngelmat.tools.yelpreviewsscraper.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ngelmat.tools.yelpreviewsscraper.domain.Author;
import net.ngelmat.tools.yelpreviewsscraper.domain.Rating;
import net.ngelmat.tools.yelpreviewsscraper.domain.Review;
import net.ngelmat.tools.yelpreviewsscraper.service.ExtractService;
import net.ngelmat.tools.yelpreviewsscraper.service.GoogleVisionService;
import net.ngelmat.tools.yelpreviewsscraper.utils.ScrapUtils;


@Service("extractService")
public class ExtractServiceImpl implements ExtractService {
  private static final String YELP_BASE_URL = "https://www.yelp.com";
  @Autowired
  private GoogleVisionService googleService;
  private static final Logger LOGGER = LoggerFactory.getLogger(ExtractServiceImpl.class);

  @Override
  public List<Review> listReviews(String url) throws IOException {
    Document doc = ScrapUtils.constructJsoupDoc(url);
    List<Review> reviews = extractReviews(doc);
    LOGGER.info("Review size: {}, url: {}", reviews.size(), url);

    return reviews;
  }

  private List<Review> extractReviews(Document doc)  {
    List<Element> reviewElements = doc.select(".review.review--with-sidebar");
    List<Review> reviews = reviewElements.stream().map(review -> {
      Author author = null;
      Rating rating = null;
      if (isReview(review)) {
        author = extractAuthor(review);
        rating = extractRating(review);
      }
      return new Review(author, rating);
    }).collect(Collectors.toList());
    reviews = reviews.stream().filter(review -> review.getAuthor() != null).collect(Collectors.toList());
    return reviews;
  }

  private Author extractAuthor(Element element) {
    Author author = new Author();
    author.setName(element.select(".user-name a").html());
    author.setProfileLink(YELP_BASE_URL + element.select(".user-name a").attr("href"));
    author.setAvatarSmallUrl(element.select("img.photo-box-img").first().attr("src"));
    author.setLocation(element.select(".user-location b").html());
    author.setFriendsCount(toInteger(element.select(".friend-count b").html()));
    author.setReviewsCount(toInteger(element.select(".review-count b").html()));
    author.setPhotosCount(toInteger(element.select(".photo-count b").html()));
    author.setElite(element.select(".is-elite").isEmpty() ? "" : element.select(".is-elite a").html());
    retrieveBigProfilePhoto(author);
    detectFaceProfile(author);

    return author;
  }

  private Rating extractRating(Element element) {
    Rating rating = new Rating();
    rating.setStars(toInteger(element.select(".i-stars").attr("title").isEmpty() ? ""
        : element.select(".i-stars").attr("title").replace(".0 star rating", "")));
    rating.setDatePosted(element.select(".rating-qualifier").html());
    rating.setMessage(element.select(".review-content p").html());

    return rating;
  }

  private boolean isReview(Element element) {
    return !element.hasClass("js-war-widget");
  }

  private static int toInteger(String str) {
    return str.isEmpty() ? 0 : Integer.valueOf(str);
  }

  private void retrieveBigProfilePhoto(Author author) {
    Document doc = null;
    try {
      doc = ScrapUtils.constructJsoupDoc(author.getProfileLink());
    } catch (IOException e) {
      LOGGER.error("Error encountered when getting profile photo: {}", author.getProfileLink(), e);
    }
    author.setAvatarUrl(doc.select("img.photo-box-img").first().attr("src"));
    author.setProfilePhotoUrl(doc.select("img.photo-box-img").first().attr("src"));
  }

  private void detectFaceProfile(Author author) {
    try {
      author.setFaceDetection(googleService.getEmotion(author.getProfilePhotoUrl()));
    } catch (IOException e) {
      LOGGER.error("Error encountered when trying to detect face profile via google: {}", author.getProfileLink(), e);
    }
  }
}
