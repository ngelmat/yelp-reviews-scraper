package net.ngelmat.tools.yelpreviewsscraper.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "google-api")
public class GoogleApiConfig {
	private static final String PARAM_API_KEY = "<API_KEY>";

	private String visionEndpointUrl;
	private String key;

	public String getVisionEndpointUrl() {
		return visionEndpointUrl.replace(PARAM_API_KEY, key);
	}

	public void setVisionEndpointUrl(String visionEndpointUrl) {
		this.visionEndpointUrl = visionEndpointUrl;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
