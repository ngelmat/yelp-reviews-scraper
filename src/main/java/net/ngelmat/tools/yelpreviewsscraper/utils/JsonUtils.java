package net.ngelmat.tools.yelpreviewsscraper.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {
	private static ObjectMapper mapper = new ObjectMapper();
	public static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);
	private static JsonFactory jf = new JsonFactory();

	public static String toJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			LOGGER.error("toJson encountered error exception.", e);
			return "";
		}
	}

	public static <T> T fromJson(String jsonAsString, Class<T> pojoClass) {
		try {
			return mapper.readValue(jsonAsString, pojoClass);
		} catch (IOException e) {
			LOGGER.error("fromJson encountered error exception.", e);
			return null;
		}
	}

	public static <T> T fromJson(FileReader fr, Class<T> pojoClass) throws JsonParseException, IOException {
		return mapper.readValue(fr, pojoClass);
	}

	public static <T> List<T> fromJsonList(String stringList, Class<T> tClass) {
		try {
			return mapper.readValue(stringList, mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass));
		} catch (IOException e) {
			LOGGER.error("fromJson encountered error exception.", e);
			return new ArrayList<>();
		}
	}

	public static String toJson(Object pojo, boolean prettyPrint) throws JsonMappingException, JsonGenerationException, IOException {
		StringWriter sw = new StringWriter();
		JsonGenerator jg = jf.createGenerator(sw);
		if (prettyPrint) {
			jg.useDefaultPrettyPrinter();
		}
		mapper.writeValue(jg, pojo);
		return sw.toString();
	}

	public static void toJson(Object pojo, FileWriter fw, boolean prettyPrint) throws JsonMappingException, JsonGenerationException, IOException {
		JsonGenerator jg = jf.createGenerator(fw);
		if (prettyPrint) {
			jg.useDefaultPrettyPrinter();
		}
		mapper.writeValue(jg, pojo);
	}

	public static void writeFile(Object pojo, String filePath) throws IOException {
		mapper.writeValue(new File(filePath), pojo );
	}
}

