package net.ngelmat.tools.yelpreviewsscraper.utils;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ScrapUtils {

  public static Document constructJsoupDoc(String url) throws IOException {
    return Jsoup.connect(url).timeout(60000).maxBodySize(10*1024*1024).get();  // to upgrade body request size of jsoup (default 1MB)
  }
}
