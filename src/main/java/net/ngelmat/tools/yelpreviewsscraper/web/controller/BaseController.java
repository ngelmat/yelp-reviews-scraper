package net.ngelmat.tools.yelpreviewsscraper.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ngelmat.tools.yelpreviewsscraper.web.model.ApiResponse;

public class BaseController {

  @ExceptionHandler(MissingServletRequestParameterException.class)
  @ResponseBody
  public Object validateFailed(MissingServletRequestParameterException e, HttpServletRequest request) {
    return ApiResponse.failed(e.getMessage());
  }

  @ExceptionHandler(IOException.class)
  @ResponseBody
  public Object validateFailed(IOException e, HttpServletRequest request) {
    return ApiResponse.failed(e.getMessage());
  }
}
