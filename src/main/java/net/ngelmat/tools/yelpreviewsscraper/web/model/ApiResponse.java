package net.ngelmat.tools.yelpreviewsscraper.web.model;

public class ApiResponse {
	private String message;
	private Object data;
	private boolean success;

	public ApiResponse(boolean success) {
		this.success = success;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public Object getData() {
		return data;
	}

	public boolean isSuccess() {
		return success;
	}

	public static final ApiResponse success() {
		ApiResponse result = new ApiResponse(true);
		result.success = true;
		return result;
	}

	public static final ApiResponse success(Object data) {
		ApiResponse result = new ApiResponse(true);
		result.setData(data);
		return result;
	}

	public static final ApiResponse success(Object data, String message) {
		ApiResponse result = new ApiResponse(true);
		result.setData(data);
		result.setMessage(message);
		return result;
	}

	public static final ApiResponse success(String message) {
		ApiResponse result = new ApiResponse(true);
		result.setMessage(message);
		return result;
	}

	public static final ApiResponse failed(String message) {
		ApiResponse result = new ApiResponse(false);
		result.setMessage(message);
		return result;
	}

	public static final ApiResponse failed(Object data, String message) {
		ApiResponse result = new ApiResponse(false);
		result.setMessage(message);
		result.setData(data);
		return result;
	}


}