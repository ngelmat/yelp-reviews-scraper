package net.ngelmat.tools.yelpreviewsscraper.web.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.ngelmat.tools.yelpreviewsscraper.service.ExtractService;
import net.ngelmat.tools.yelpreviewsscraper.web.model.ApiResponse;

@RequestMapping("/api")
@RestController
public class ApiController extends BaseController {

  @Autowired
  private ExtractService extractService;

  @GetMapping("/test")
  public String test() {
    return "test";
  }

  @GetMapping("/scrap")
  public ApiResponse scrap(@RequestParam("url") String url) throws IOException {
    return ApiResponse.success(extractService.listReviews(url), "Yelp reviewers list successfully.");
  }
}
