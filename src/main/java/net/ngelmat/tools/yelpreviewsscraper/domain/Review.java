package net.ngelmat.tools.yelpreviewsscraper.domain;

public class Review {

  private Author author;
  private Rating rating;

  public Review(Author author, Rating rating) {
    this.author = author;
    this.rating = rating;
  }

  public Author getAuthor() {
    return author;
  }

  public Rating getRating() {
    return rating;
  }
}
