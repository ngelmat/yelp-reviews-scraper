package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VisionResponse {

	private List<ImgAnnotation> faceAnnotations;

	public List<ImgAnnotation> getFaceAnnotations() {
		return faceAnnotations;
	}

	public void setFaceAnnotations(List<ImgAnnotation> faceAnnotations) {
		this.faceAnnotations = faceAnnotations;
	}
}
