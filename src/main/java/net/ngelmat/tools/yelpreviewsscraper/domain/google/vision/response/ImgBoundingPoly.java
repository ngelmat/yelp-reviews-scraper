package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImgBoundingPoly {
	List<ImgVertex> vertices;

	public List<ImgVertex> getVertices() {
		return vertices;
	}

	public void setVertices(List<ImgVertex> vertices) {
		this.vertices = vertices;
	}
}
