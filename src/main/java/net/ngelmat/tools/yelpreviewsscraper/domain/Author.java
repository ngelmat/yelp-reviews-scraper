package net.ngelmat.tools.yelpreviewsscraper.domain;

import net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response.VisionResponse;

public class Author {
  private String name;
  private String profileLink;
  private String avatarSmallUrl;
  private String avatarUrl;
  private String profilePhotoUrl;
  private String location;
  private int friendsCount;
  private int reviewsCount;
  private int photosCount;
  private String elite = "";
  private VisionResponse faceDetection;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProfileLink() {
    return profileLink;
  }

  public void setProfileLink(String profileLink) {
    this.profileLink = profileLink;
  }

  public String getAvatarSmallUrl() {
    return avatarSmallUrl;
  }

  public void setAvatarSmallUrl(String avatarSmallUrl) {
    this.avatarSmallUrl = avatarSmallUrl;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public String getProfilePhotoUrl() {
    return profilePhotoUrl;
  }

  public void setProfilePhotoUrl(String profilePhotoUrl) {
    this.profilePhotoUrl = profilePhotoUrl;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getFriendsCount() {
    return friendsCount;
  }

  public void setFriendsCount(int friendsCount) {
    this.friendsCount = friendsCount;
  }

  public int getReviewsCount() {
    return reviewsCount;
  }

  public void setReviewsCount(int reviewsCount) {
    this.reviewsCount = reviewsCount;
  }

  public int getPhotosCount() {
    return photosCount;
  }

  public void setPhotosCount(int photosCount) {
    this.photosCount = photosCount;
  }

  public String getElite() {
    return elite;
  }

  public void setElite(String elite) {
    this.elite = elite;
  }

  public VisionResponse getFaceDetection() {
    return faceDetection;
  }

  public void setFaceDetection(VisionResponse faceDetection) {
    this.faceDetection = faceDetection;
  }
}
