package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoogleApiResponse {

	private List<VisionResponse> responses;

	public List<VisionResponse> getResponses() {
		return responses;
	}

	public void setResponses(List<VisionResponse> responses) {
		this.responses = responses;
	}
}
