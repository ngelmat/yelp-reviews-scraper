package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

public class ImageSource {
	private String imageUri;

	public ImageSource(String imageUri) {
		this.imageUri = imageUri;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}
}
