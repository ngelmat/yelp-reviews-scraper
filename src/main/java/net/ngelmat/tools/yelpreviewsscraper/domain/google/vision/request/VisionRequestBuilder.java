package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

import java.util.Arrays;

public class VisionRequestBuilder {
	private static final String FACE_DETECTION_TYPE = "FACE_DETECTION";

	public static VisionRequest createEmotionRequest(String imageUrl) {
		VisionRequest vision = new VisionRequest();
		vision.getRequests().add(new ImageRequest(new PictureImage(new ImageSource(imageUrl)), Arrays.asList(new ImageFeature(FACE_DETECTION_TYPE, 50))));

		return vision;
	}
}
