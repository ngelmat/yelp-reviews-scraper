package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

import java.util.List;

public class ImageRequest {
	private PictureImage image;
	private List<ImageFeature> features;

	public ImageRequest(PictureImage image, List<ImageFeature> features) {
		this.image = image;
		this.features = features;
	}

	public PictureImage getImage() {
		return image;
	}

	public List<ImageFeature> getFeatures() {
		return features;
	}
}
