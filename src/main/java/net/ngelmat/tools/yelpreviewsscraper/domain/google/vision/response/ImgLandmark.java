package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImgLandmark {
	private String type;
	private LandMarkPosition position;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LandMarkPosition getPosition() {
		return position;
	}

	public void setPosition(LandMarkPosition position) {
		this.position = position;
	}
}
