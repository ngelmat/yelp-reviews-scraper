package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

public class ImageFeature {
	private String type;
	private int maxResults;

	public ImageFeature(String type, int maxResults) {
		this.type = type;
		this.maxResults = maxResults;
	}

	public String getType() {
		return type;
	}

	public int getMaxResults() {
		return maxResults;
	}
}
