package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

import java.util.ArrayList;
import java.util.List;

public class VisionRequest {
	private List<ImageRequest> requests = new ArrayList<>();


	public List<ImageRequest> getRequests() {
		return requests;
	}
}
