package net.ngelmat.tools.yelpreviewsscraper.domain.google.vision.request;

public class PictureImage {
	private ImageSource source;

	public PictureImage(ImageSource source) {
		this.source = source;
	}

	public ImageSource getSource() {
		return source;
	}
}
