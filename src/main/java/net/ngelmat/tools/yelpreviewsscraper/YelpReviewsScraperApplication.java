package net.ngelmat.tools.yelpreviewsscraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YelpReviewsScraperApplication {

  public static void main(String[] args) {
    SpringApplication.run(YelpReviewsScraperApplication.class, args);
  }

}

